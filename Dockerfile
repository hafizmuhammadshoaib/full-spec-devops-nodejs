FROM node:12 AS ui-build
WORKDIR /frontend
COPY client .
RUN npm install && npm run build

FROM node:12 AS server-build
WORKDIR /backend
COPY --from=ui-build /frontend/build ./client/build
COPY package.json .
RUN  npm install
COPY server.js .
ENV NODE_ENV production
EXPOSE 5000

CMD ["node", "server.js"]
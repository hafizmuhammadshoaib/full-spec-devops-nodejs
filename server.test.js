const { expect } = require('chai');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('./server');


// Configure chai
chai.use(chaiHttp);
chai.should();



describe("Test", () => {
    describe("GET /api/hello", () => {
        it("hello from express", (done) => {
            chai.request(app)
                .get('/api/hello')
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.express).to.be.equal('Hello From Express')
                    done();
                });
        });

    })
    describe("POST /api/world", () => {
        it("post message", (done) => {
            chai.request(app)
                .post('/api/world').send({ post: 'hello world' })
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.text).to.be.equal('I received your POST request. This is what you sent me: hello world')
                    done();
                });
        });

    })
})
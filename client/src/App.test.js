import React from 'react';
import App from './App';
import { mount, shallow } from 'enzyme';


global.fetch = jest.fn(() => Promise.resolve(
  {
    json: () => Promise.resolve({ express: 'Hello From Express' }),
    status: 200
  }
)
)
const flushPromises = () => new Promise(setImmediate);


describe('test', () => {
  it('get anchor tag', () => {
    const wrapper = mount(<App />)
    let text = wrapper.find('a').text()
    expect(text).toBe('Learn React')
  })
  it('mock get api', async () => {
    const wrapper = mount(<App />)
    await flushPromises();
    expect(wrapper.state('response')).toBe('Hello From Express')
  })

  // it('get anchor tag failed test', () => {
  //   const wrapper = mount(<App />)
  //   let text = wrapper.find('a').text()
  //   expect(text).toBe('Learn React!')
  // })
})
